# README #

Prueba Técnica Desarrollador Junior

Lenguaje Python

En cualquier lenguaje de programación moderno, cree una función que itera a través de un arreglo de objetos (primer argumento)
y devuelve un arreglo de todos los objetos que tienen el par propiedad valor coincidente (segundo argumento).
La propiedad del segundo argumento debe estar presente en todos los objetos del arreglo.

Ejecutada y probada desde main.py con los datos:

primer argumento es: [{nombre: "John", apellido: "Doe"}, {nombre: "Jane", apellido: null}, {nombre: "Jose", apellido: "Doe"}]
Y el segundo argumento es: {apellido: "Doe"}